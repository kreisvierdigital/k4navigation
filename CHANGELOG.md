# Changelog

## 2.3.3 - 2021-08-02

### Changed
- k4NavigationGetSubNavOnly supports filter and nac classes now (as parameters)

## 2.3.0 - 2021-01-05

### Changed
- consolidated codebase

## 2.2.99 - 2020-12-10

### Added
- Added k4NavigationGetFirstNavOnly and k4NavigationGetSubNavOnly functions to allow generation of separated navigation lists for easier overlaying and fullscreen navigations

## 2.2.1 - 2019.06.18

### Added
- RegEx fix for PHP 7.3+: updated Simple HTML DOM to version 1.9 (was 1.5)

## 2.0.0 - 2017.03.16

### Added
- Initial craft 3 release
