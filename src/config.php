<?php
/**
 *  k4navigationCraft3 plugin for Craft CMS 3.x
 *
 * Craft Plugin Twig filter for advanced navigations in Craft CMS.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-21 Thomas Bauer
 * 
 * @author    Thomas Bauer
 * @package   K4navigationcraft3
 * @since     2.3.0
 */


return [

    // This controls blah blah blah
    "k4Setting" => true,

];
