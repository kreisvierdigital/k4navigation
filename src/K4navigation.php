<?php
/**
 *  k4navigationCraft3 plugin for Craft CMS 3.x
 *
 * Craft Plugin Twig filter for advanced navigations in Craft CMS.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-21 Thomas Bauer
 * 
 * @author    Thomas Bauer
 * @package   k4Navigation
 * @since     2.3.3
 */

namespace k4\k4navigation;


use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use k4\k4navigation\twigextensions\k4NavigationTwigExtension;

use yii\base\Event;


class K4navigation extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var K4navigation
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            Plugins::className(),
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        require_once "library/simple_html_dom.php";


        // Add in our Twig extensions
        Craft::$app->view->twig->addExtension(new k4NavigationTwigExtension());

        Craft::info(
          Craft::t(
            'k4-navigation',
            '{name} plugin loaded',
            ['name' => $this->name]
          ),
          __METHOD__
        );

    }

}
