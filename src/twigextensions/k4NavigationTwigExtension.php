<?php 
/**
 *  k4navigationCraft3 plugin for Craft CMS 3.x
 *
 * Craft Plugin Twig filter for advanced navigations in Craft CMS.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-21 Thomas Bauer
 * 
 * @author    Thomas Bauer
 * @package   k4Navigation
 * @since     2.3.3
 */

namespace k4\k4navigation\twigextensions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class k4NavigationTwigExtension extends AbstractExtension
{
    public function getName()
    {
        return 'k4 navigation plugin';
    }

    public function getFilters()
    {
        $returnArray = array();
        $methods = array(
            'k4NavigationGetActivePath',
            'k4NavigationGetBreadcrumb',
            'k4NavigationGetSimpleNavigation',
            'k4NavigationShowFromTo',
            'k4NavigationGetNextSubmenu',
            'k4NavigationGetBreadcrumbLi',
            'k4NavigationGetFirstNavOnly',
            'k4NavigationGetSubNavOnly'
        );
        foreach ($methods as $methodName) {
            
            $returnArray[$methodName] = new TwigFilter($methodName, [$this, $methodName]);
        }
        return $returnArray;
    }  


    public function k4NavigationGetActivePath($content,$selectedPath,$activeClass = "active")
    {
        $html = str_get_html($content);
       
        $selectedFilter = 'a[href="'.$selectedPath.'"]';

        //set selected link 
        $selectElemArr = $html->find($selectedFilter);

        if (!empty($selectElemArr)){

            $selectElem = $selectElemArr[0];

           // Element wurde gefunden - Active Klassen setzen
            for ($i = 1; $i <= 20; $i++) {
                $attrs = str_replace($activeClass, "", $selectElem->getAttribute("class"));
                $selectElem->setAttribute("class", $attrs." ". $activeClass);
                $selectElem =  $selectElem->parent();
                if (!is_object($selectElem)){
                    break;
                }
            }
        } else {
            $html = $content . "<!-- k4-navigation no link Active -->";
        }
        
        $html = str_get_html($html);
        $selectedFilter = 'li ul';

        //set selected link 
        $selectElemArr = $html->find($selectedFilter);
        //löschen aller leeren ULs, hinzufügen von hasChildren-Klasse bei übergeordneten li Elementen
        if (!empty($selectElemArr)){
            
            foreach ($selectElemArr as $selectElem) {
                if ($selectElem->children){
                    $selectElem =  $selectElem->parent();
                    $attrs = str_replace("hasChildren", "", $selectElem->getAttribute("class"));
                    $selectElem->setAttribute("class", $attrs." hasChildren");
                } else{
                    $selectElem->outertext = '';
                }
            }
            $selectElem = $selectElemArr[0];
        }
        
        return $html;
    }

    public function k4NavigationGetBreadcrumb($content,$selectedPath,$dividerText = " > ")
    {
        $html = str_get_html($this->k4NavigationGetActivePath($content,$selectedPath));
        
        $breadcrumb = "";

           foreach($html->find('li.active') as $element){
              $elementLink = $element->find("a")[0];
                if (false !== mb_strpos($elementLink->class, "active")){
                    $breadcrumb = $breadcrumb. $elementLink->innertext;
                }else{
                    $breadcrumb = $breadcrumb . $elementLink->outertext . $dividerText;
                }
        }

        return $breadcrumb;

    }

    public function k4NavigationGetSimpleNavigation($content,$selectedPath)
    {
        $html = str_get_html($this->k4NavigationGetActivePath($content,$selectedPath));
        
        $filter = 'ul li[class!="active"] ul,ul li[!class] ul';
        foreach($html->find($filter) as $element){
                  $element->outertext = "";
        }
        return $html;        
    }  
    
    public function k4NavigationGetFirstNavOnly($content, $selectedPath, $filterClass)
    {
        $html = str_get_html($this->k4NavigationGetActivePath($content,$selectedPath));
        $filter = $filterClass . '[data-ul-id!=""]';
        foreach($html->find($filter) as $element){
                  $element->outertext = "";
        }
        return $html;        
    }  
    
    public function k4NavigationGetSubNavOnly($content, $selectedPath, $filterClass = 'ul ul.level2', $navClass = 'subnav--menu')
    {
        $html = str_get_html($this->k4NavigationGetActivePath($content,$selectedPath));
        $subnavs = "";
        $filter = $filterClass . '[data-ul-id!=""]';
        foreach($html->find($filter) as $element){
                 $subnavs = $subnavs ."<nav class=\"" . $navClass . "\" data-nav-id=\"" . $element->getAttribute('data-ul-id') . "\">". $element->outertext . "</nav>" ;
        }
        return $subnavs;        
    }  
    
    public function k4NavigationShowFromTo($content,$levelFrom,$levelTo)
    {   

        $html = str_get_html($content);
        $html2 = "";
        
        if ($levelFrom > 0 AND !empty($html)){
            $filter = '.level'.$levelFrom;
            foreach($html->find($filter) as $element){
                      $html2 = $element->outertext;
            }
        }
        $html = str_get_html($html2);
        if ($levelTo > 0 AND !empty($html)){
            $filter = '.level'.$levelTo;
            foreach($html->find($filter) as $element){
                      $element->outertext = "";
            }
        }
        
        return $html;        
    }
    
    public function k4NavigationGetNextSubmenu($content,$selectedPath)
    {   
        //$time_pre = microtime(true);

        $html2 = "";
        
        $html = str_get_html($content);
       
        $selectedFilter = 'a[href="'.$selectedPath.'"]';

        //set selected link 
        $selectElemArr = $html->find($selectedFilter);//->parent();
        if (is_array($selectElemArr) AND !empty($selectElemArr[0])){
            $html2 = $selectElemArr[0]->next_sibling();
            if (!empty($html2)){
                foreach($html2->find("ul") as $element){
                          $element->outertext = "";
                }
                $html2->outertext;    
            }

        }
        
        //$time_post = microtime(true);
        //$exec_time = $time_post - $time_pre;
        //return $exec_time . "<br />" . $html2;        
      
        return $html2;
    }
  
    public function k4NavigationGetBreadcrumbLi($content,$selectedPath,$dividerText = " > ",$linkActive = false)
    {
        $breadcrumb = "";
        
        $html = str_get_html($this->k4NavigationGetActivePath($content,$selectedPath));
        
        foreach($html->find('li.active') as $element) {
            $elementLink = $element->find("a")[0];
            $elementLink->class = $elementLink->class . "breadcrumb__list--link";
            if (false !== mb_strpos($elementLink->class, "active")) {
                if ($linkActive){
                    $breadcrumb = $breadcrumb . '<li class="breadcrumb__list--item">' . $elementLink->outertext . '</li>';
                } else {
                    $breadcrumb = $breadcrumb. '<li class="breadcrumb__list--item">' . $elementLink->innertext . '</li>';
                }

            }else{
                $breadcrumb = $breadcrumb . '<li class="breadcrumb__list--item">' . $elementLink->outertext . '</li>' . $dividerText;
            }
        }
        return $breadcrumb;
    }
}
