<?php
/**
 *  k4navigationCraft3 plugin for Craft CMS 3.x
 *
 * Craft Plugin Twig filter for advanced navigations in Craft CMS.
 *
 * @link      http://www.kreisvier.ch
 * @copyright Copyright (c) 2017-21 Thomas Bauer
 * 
 * @author    Thomas Bauer
 * @package   k4Navigation
 * @since     2.3.3
 */
return [
    'plugin loaded' => 'plugin loaded',
];
