#  k4navigation plugin for Craft CMS 3.x

Craft Plugin Twig filter for advanced navigations in Craft CMS.

![Screenshot](resources/img/plugin-logo.png)

## Installation

To install  k4navigationCraft3, follow these steps:

1.  Install with Composer via `composer require k4/k4navigation` from your project directory.


k4navigation works with Craft 3.0 or later.

##  k4navigation Overview

Twig filter for advanced navigations in Craft CMS.

## Using  k4navigation

First, set your Menu in craft.

    {% set entries = craft.entries()
      .section('pages')
      .level(1)
      .with(['descendants'])
      .all() 
    %}

    {% set mainnavigation %}

        {% for entry in entries %}
            <li>
                <a href="{{ entry.url }}">
                    {{ entry.title }}
                    {% if entry.hasDescendants %}
                        <i class="fas fa-caret-down"></i>
                    {% endif %}
                </a>
                {% nav node in entry.descendants %}
                    ...
                {% endnav %}
            </li>
        {% endfor %}

    {% endset %}

Now you can use the k4-navigation twig filter. See the examples below:

###Simple add class "active" to all Parent-Elements of selected item
        {{ mainnavigation | k4NavigationGetActivePath(url(craft.app.request.pathInfo)) | raw }}


###Simple Menu shows only first level and also selected path
        {{ mainnavigation | k4NavigationGetSimpleNavigation(url(craft.app.request.pathInfo)) | raw }}


###Show Breadcrumb for menu
        {{ mainnavigation | k4NavigationGetBreadcrumb(url(craft.app.request.pathInfo)," > ") | raw }}


###Generate Navigation menu with first level navigation only 
        {{ mainnavigation | k4NavigationGetFirstNavOnly(craft.app.request.absoluteUrl) | raw }}


###Generate Navigation menu excluding first level items (submenu only)
        {{ mainnavigation | k4NavigationGetSubNavOnly(craft.app.request.absoluteUrl) | raw }}



Brought to you by [Thomas Bauer](http://www.kreisvier.ch)
